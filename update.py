#!/usr/bin/env python3
import json
import os
import sys
import datetime
import subprocess
import platform
import shutil
import socket
from subprocess import run

TERMINALS = [
    "konsole",
    "yakuake",
    "foot",
    "ghostty",
    "kitty",
    "alacritty",
    "gnome-terminal",
    "urxvt",
    "xterm",
    "windows terminal",
    "wezterm",
]
BROWSERS = [
    "firefox",
    "microsoft edge",
    "brave",
    "brave-browser",
    "chromium",
    "chromium-browser",
    "chrome",
]
EDITORS = ["code", "notepad", "notepad++", "emacs", "vim", "nvim", "hx"]

PACKAGE_MANAGERS = ["apt-get", "pacman", "dnf", "nix", "zypper"]


def write_json(new_data, filename="data.json"):
    try:
        with open(filename, "r+") as file:
            file_data = json.load(file)
            file_data["data"].insert(0, new_data)
            file.seek(0)
            json.dump(file_data, file, indent=4)
    except FileNotFoundError:
        print("File does not exist - update failed!")
        sys.exit(1)


def set_running_streak(hostname, filename="data.json"):
    try:
        with open(filename, "r") as file:
            file_data = json.load(file)
            if file_data:
                data = {}
                ind = 0
                for ind, d in enumerate(file_data["data"]):
                    if d["hostname"] == hostname and d["days"] == "Still running":
                        data = calulate_running_streak(d)
                        break
                file_data["data"][ind] = data
                with open(filename, "w") as wfile:
                    json.dump(file_data, wfile, indent=4)
    except FileNotFoundError:
        print("File does not exist - update failed!")
        sys.exit(1)


def calulate_running_streak(data):
    fmt = "%Y-%m-%d %H:%M"
    now = datetime.datetime.now().strftime(fmt)

    tdelta = datetime.datetime.strptime(now, fmt) - datetime.datetime.strptime(
        data["time"], fmt
    )

    if tdelta.days < 0:
        data["days"] = 0
    else:
        data["days"] = tdelta.days

    return data


def get_operating_system():
    os_id = platform.system()
    if os_id == "Linux":
        try:
            os.stat("/etc/fedora-release")
            return read_file("/etc/fedora-release").strip()
        except FileNotFoundError:
            pass

        try:
            os.stat("/etc/os-release")
            name = parse_os_release()
            if name:
                return name
        except FileNotFoundError:
            pass

        import distro

        return distro.id()
    elif os_id == "Windows":
        return platform.platform()


def read_file(filename):
    with open(filename, "r") as file:
        return file.read()


def parse_os_release():
    c = read_file("/etc/os-release")
    for line in c.split("\n"):
        if line.lower().startswith("name"):
            return line.split("=")[1].strip('"')


def lookup_applications(applications):
    res = ""
    for app in applications:
        if application_exists(app):
            if app == "hx":
                app = "helix"
            res += app + ", "

    if res.endswith(", "):
        res = res[:-2]

    return res


def get_kernel_version():
    if platform.system() != "Linux":
        return ""
    command = "uname -r"
    result = subprocess.check_output(command, shell=True).decode("utf-8").strip()
    return result


def application_exists(application):
    if platform.system() == "Windows":
        # The shutil is much faster, but does not find all applications
        if shutil.which(application):
            return True

        p = run(["winget", "list", "--name", application], stdout=subprocess.DEVNULL)
        return p.returncode == 0
    return shutil.which(application)


def get_environment_variable(env):
    res = os.getenv(env)
    if res is None:
        return ""
    return res


if __name__ == "__main__":
    data = {
        "time": datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),
        "hostname": socket.gethostname(),
        "os": get_operating_system(),
        "kernel": get_kernel_version(),
        "de": get_environment_variable("XDG_CURRENT_DESKTOP"),
        "session": get_environment_variable("XDG_SESSION_TYPE"),
        "pkgmgr": lookup_applications(PACKAGE_MANAGERS),
        "browser": lookup_applications(BROWSERS),
        "terminal": lookup_applications(TERMINALS),
        "editor": lookup_applications(EDITORS).replace(
            "emacs", "doom-emacs"
        ),  # lazy, lazy lazy!!
        "days": "Still running",
    }

    if data["de"] == "":
        data["de"] = get_environment_variable("XDG_SESSION_DESKTOP")

    set_running_streak(data["hostname"])

    write_json(data)
