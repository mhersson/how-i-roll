{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
  name = "how-I-roll";
  buildInputs = [ pkgs.python3Packages.distro ];
  shellHook = ''
    echo "Now run python update.py"
  '';
}
