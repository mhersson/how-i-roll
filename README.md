[![Made with Doom Emacs](https://img.shields.io/badge/Made_with-Doom_Emacs-blueviolet.svg?style=flat-square&logo=GNU%20Emacs&logoColor=white)](https://github.com/hlissner/doom-emacs)
[![Netlify Status](https://api.netlify.com/api/v1/badges/ba557570-9e90-46e6-92de-315159333872/deploy-status)](https://app.netlify.com/sites/howiroll/deploys)
-------
## How I roll

This is just a little joke between a colleague of mine, Jon, and me. I tend to
reinstall my machines a lot, either to test the next shiny new thing, or because
I am tired of the old environment and need a change, or I am just bored and
would like a clean install. Jon joked about how I should create a page with an
install log so he could follow, and didn't have to ask every week(day)
how-I-roll. So here it is!

[This is for you, Jon](https://howiroll.netlify.app/) 🙂
